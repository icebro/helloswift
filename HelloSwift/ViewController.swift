//
//  ViewController.swift
//  HelloSwift
//
//  Created by 林順斌 on 2019/6/22.
//  Copyright © 2019年 林順斌. All rights reserved.
//

import UIKit
import GameplayKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        print("Test Entry Point");
        
        // test NetConn API
        let auth = netConnApiAuth("https://www.ope18.com/service/API/Market/ListAsync");
        print("auth:\(auth)")
    }
    
    // Netconn API Auth
    private func netConnApiAuth(_ url: String) -> String {
        // 從 NetConn 取得授權碼
        let authToken  = "593d3fd7d34f47d0ae84a4c92133706f";
        // Unixtime + 60 秒的時間
        //String expTime = String.valueOf(System.currentTimeMillis() / 1000L + 60);
        let expTime = Int(currentUnixTime()) + 60;
        print("expTime:\(expTime)");
        
        var nonce : String = "";
        nonce = nonce.randomString(length:10);
        print("nonce : \(nonce)");

        // 將上列變數組合
        //String fullString = String.format("NCS-Auth|%s|%s|%s|%s", authToken, expTime, nonce, url
        var  fullString = "NCS-Auth|\(authToken)|\(expTime)|\(nonce)|\(url)";
        print("fullString:\(fullString)");
        
        // 生成一個 MD5 加密
        //MessageDigest md = MessageDigest.getInstance("MD5");
        // 計算 MD5 函數
        //md.update(fullString.getBytes());
        let signature = fullString.utf8.md5
        print("md5: \(signature)")
        
        //ret = String.format("%s|%s|%s", expTime, nonce, signature);
        let ret = "\(expTime)|\(nonce)|\(signature)";
        print("ret:\(ret)");
        return ret;
    }
    
    private func currentUnixTime() -> TimeInterval {
        //当前时间的时间戳
        let timeInterval:TimeInterval = Date().timeIntervalSince1970
        return timeInterval;
    }
}
// Data extension hexEncodeString
extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}

extension String {
    func randomString(length : Int) -> String {
        let charSet = Array("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: charSet) as! [Character]
        let array = shuffled.prefix(length)
        return String(array)
    }
}
